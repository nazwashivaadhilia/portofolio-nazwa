<!DOCTYPE html>
<html lang="en">

<head>
    <title>Nazwa Shiva Adhilia</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="asset-frontend/css/animate.css">

    <link rel="stylesheet" href="asset-frontend/css/owl.carousel.min.css">
    <link rel="stylesheet" href="asset-frontend/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="asset-frontend/css/magnific-popup.css">

    <link rel="stylesheet" href="asset-frontend/css/flaticon.css">
    <link rel="stylesheet" href="asset-frontend/css/style.css">
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">


    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light site-navbar-target"
        id="ftco-navbar">
        <div class="container">
            <a class="navbar-brand" href="index.html">Nazwa<span>.</span></a>
            <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse"
                data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="oi oi-menu"></span> Menu
            </button>

            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav nav ml-auto">
                    <li class="nav-item"><a href="#home-section" class="nav-link"><span>Home</span></a></li>
                    <li class="nav-item"><a href="#about-section" class="nav-link"><span>About</span></a></li>
                    <li class="nav-item"><a href="#skills-section" class="nav-link"><span>Skills</span></a></li>
                    <li class="nav-item"><a href="#projects-section" class="nav-link"><span>Projects</span></a></li>
                    <li class="nav-item"><a href="#contact-section" class="nav-link"><span>Contact</span></a></li>
                    {{-- <li class="nav-item"><a href="#contact-section" class="nav-link"><span>Contact</span></a></li> --}}
                </ul>
            </div>
        </div>
    </nav>
    <section id="home-section" class="hero">
        <div class="home-slider owl-carousel">
            <div class="slider-item">
                <div class="overlay"></div>
                <div class="container-fluid px-md-0">
                    <div class="row d-md-flex no-gutters slider-text align-items-end justify-content-end"
                        data-scrollax-parent="true">
                        <div class="one-third order-md-last img"
                            style="background-image:url(asset-frontend/images/1.jpg);">
                            <div class="overlay"></div>
                            <div class="overlay-1"></div>
                        </div>
                        <div class="one-forth d-flex  align-items-center ftco-animate"
                            data-scrollax=" properties: { translateY: '70%' }">
                            <div class="text">
                                <span class="subheading">Hello! This is Nazwa</span>
                                <h1 class="mb-4 mt-3"><span>Frontend</span> Eingeneer &amp; Developer</h1>
                                <p><a href="https://www.linkedin.com/in/nazwa-shiva-adhilia-9693b0267/"
                                        class="btn btn-primary">Hire me</a> <a href="#"
                                        class="btn btn-primary btn-outline-primary">Download CV</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="slider-item">
                <div class="overlay"></div>
                <div class="container-fluid px-md-0">
                    <div class="row d-flex no-gutters slider-text align-items-end justify-content-end"
                        data-scrollax-parent="true">
                        <div class="one-third order-md-last img"
                            style="background-image:url(asset-frontend/images/2.jpg);">
                            <div class="overlay"></div>
                            <div class="overlay-1"></div>
                        </div>
                        <div class="one-forth d-flex align-items-center ftco-animate"
                            data-scrollax=" properties: { translateY: '70%' }">
                            <div class="text">
                                <span class="subheading">I'm Design &amp; Develop Brands</span>
                                <h1 class="mb-4 mt-3">Hi, I am <span>Nazwa Shiva</span> Welcome to my portofolio.</h1>
                                <p><a href="https://www.linkedin.com/in/nazwa-shiva-adhilia-9693b0267/"
                                        class="btn btn-primary">Hire me</a> <a href="#"
                                        class="btn btn-primary btn-outline-primary">Download CV</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><br><br><br>
    <section class="ftco-about ftco-section ftco-no-pt ftco-no-pb" id="about-section">
        <div class="container">
            <div class="row d-flex no-gutters">
                <div class="col-md-6 col-lg-5 d-flex">
                    <div class="img-about img d-flex align-items-stretch">
                        <div class="overlay"></div>
                        <div class="img d-flex align-self-stretch align-items-center"
                            style="background-image:url(asset-frontend/images/aboutpic.jpg);">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-7 pl-md-4 pl-lg-5 py-5">
                    <div class="py-md-5">
                        <div class="row justify-content-start pb-3">
                            <div class="col-md-12 heading-section ftco-animate">
                                <span class="subheading">My Intro</span>
                                <h2 class="mb-4" style="font-size: 34px; text-transform: capitalize;">About Me</h2>
                                <p>I am a frontend engineer and developer, passionate about fulfilling my duties in my
                                    field. I am confident in my ability to contribute effectively to the industry's
                                    success. With relevant skills and experience in web design, I have completed several
                                    projects in this field.</p>

                                <ul class="about-info mt-4 px-md-0 px-2">
                                    <li class="d-flex"><span>Name:</span> <span>Nazwa Shiva Adhilia</span></li>
                                    <li class="d-flex"><span>Date of birth:</span> <span>December 04, 2005</span></li>
                                    <li class="d-flex"><span>Address:</span> <span>Cigugur Tengah, Kota Cimahi, Bandung
                                            Barat</span></li>
                                    <li class="d-flex"><span>Zip code:</span> <span>40521</span></li>
                                    <li class="d-flex"><span>Email:</span> <span>nazwashivaadhilia@gmail.com</span>
                                    </li>
                                    <li class="d-flex"><span>Phone: </span> <span>+62 823-1559-7512</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br><br><br><br>
    <section class="ftco-hireme" id="skills-section">
        <div class="container">
            <div class="row justify-content-center pb-5">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <span class="subheading">Skills</span>
                    <h2 class="mb-4">My Skills</h2>
                    <p>Several relevant skills in the field of web design that I possess.</p>
                </div>
            </div>
            <div class="row progress-circle mb-5">
                <div class="col-lg-4 mb-4">
                    <div class="bg-white rounded-lg shadow p-4 ">
                        <h2 class="h5 font-weight-bold text-center mb-4 text-dark">CSS</h2>

                        <!-- Progress bar 1 -->
                        <div class="progress mx-auto" data-value='95'>
                            <span class="progress-left">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <div
                                class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">95<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>

                <div class="col-lg-4 mb-4">
                    <div class="bg-white rounded-lg shadow p-4">
                        <h2 class="h5 font-weight-bold text-center mb-4 text-dark">HTML</h2>

                        <!-- Progress bar 1 -->
                        <div class="progress mx-auto" data-value='95'>
                            <span class="progress-left">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <div
                                class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">95<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>

                <div class="col-lg-4 mb-4">
                    <div class="bg-white rounded-lg shadow p-4">
                        <h2 class="h5 font-weight-bold text-center mb-4 text-dark">JavaScript</h2>

                        <!-- Progress bar 1 -->
                        <div class="progress mx-auto" data-value='50'>
                            <span class="progress-left">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <div
                                class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">50<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>

                <div class="col-lg-4 mb-4">
                    <div class="bg-white rounded-lg shadow p-4">
                        <h2 class="h5 font-weight-bold text-center mb-4 td text-dark">Figma</h2>

                        <!-- Progress bar 1 -->
                        <div class="progress mx-auto" data-value='70'>
                            <span class="progress-left">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <div
                                class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">70<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>

                <div class="col-lg-4 mb-4">
                    <div class="bg-white rounded-lg shadow p-4">
                        <h2 class="h5 font-weight-bold text-center mb-4 text-dark">Laravel</h2>

                        <!-- Progress bar 1 -->
                        <div class="progress mx-auto" data-value='90'>
                            <span class="progress-left">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <div
                                class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">90<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>

                <div class="col-lg-4 mb-4">
                    <div class="bg-white rounded-lg shadow p-4">
                        <h2 class="h5 font-weight-bold text-center mb-4 text-dark">Nuxt Js</h2>

                        <!-- Progress bar 1 -->
                        <div class="progress mx-auto" data-value='50'>
                            <span class="progress-left">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar border-primary"></span>
                            </span>
                            <div
                                class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">50<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-project" id="projects-section">
        <div class="container-fluid px-md-4">
            <div class="row justify-content-center pb-5">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <span class="subheading">Accomplishments</span>
                    <h2 class="mb-4">Our Projects</h2>
                    <p>Several projects I have worked on in web design </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="project img shadow ftco-animate d-flex justify-content-center align-items-center"
                        style="background-image: url(asset-frontend/images/work-1.jpg);">
                        <div class="overlay"></div>
                        <div class="text text-center p-4">
                            <h3><a href="#">Uma Design</a></h3>
                            <span>Frontend designer - Web Design</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="project img shadow ftco-animate d-flex justify-content-center align-items-center"
                        style="background-image: url(asset-frontend/images/work-2.jpg);">
                        <div class="overlay"></div>
                        <div class="text text-center p-4">
                            <h3><a href="#">Hawwa Tours &amp; Travel</a></h3>
                            <span>Frontend eingeneer - Web Design</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="project img shadow ftco-animate d-flex justify-content-center align-items-center"
                        style="background-image: url(asset-frontend/images/work-3.jpg);">
                        <div class="overlay"></div>
                        <div class="text text-center p-4">
                            <h3><a href="#">Swimgo</a></h3>
                            <span>Frontend Eingeneer &amp; Designer</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="project img shadow ftco-animate d-flex justify-content-center align-items-center"
                        style="background-image: url(asset-frontend/images/work-4.jpg);">
                        <div class="overlay"></div>
                        <div class="text text-center p-4">
                            <h3><a href="#">Mingstore</a></h3>
                            <span>UI/UX Designer</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="ftco-hireme" id="contact-section">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-8 col-lg-8 d-flex align-items-center">
                    <div class="w-100 py-4">
                        <h2>Have a project on your mind.</h2>
                        <p>If you would like to know more about me, feel free to contact me here.</p>
                        <p class="mb-0"><a href="https://wa.me/+6282315597512"
                                class="btn btn-white py-3 px-4">Contact me</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="ftco-footer ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">

                    <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </footer>



    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4"
                stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4"
                stroke-miterlimit="10" stroke="#F96D00" />
        </svg></div>


    <script src="asset-frontend/js/jquery.min.js"></script>
    <script src="asset-frontend/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="asset-frontend/js/popper.min.js"></script>
    <script src="asset-frontend/js/bootstrap.min.js"></script>
    <script src="asset-frontend/asset-frontend/js/jquery.easing.1.3.js"></script>
    <script src="asset-frontend/js/jquery.waypoints.min.js"></script>
    <script src="asset-frontend/js/jquery.stellar.min.js"></script>
    <script src="asset-frontend/js/owl.carousel.min.js"></script>
    <script src="asset-frontend/js/jquery.magnific-popup.min.js"></script>
    <script src="asset-frontend/js/jquery.animateNumber.min.js"></script>
    <script src="asset-frontend/js/scrollax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="asset-frontend/js/google-map.js"></script>

    <script src="asset-frontend/js/main.js"></script>

</body>

</html>
